variable "vpc_cidr" {
    description = "For VPC"
    type = string
    default = "192.168.0.0/16"
}

variable "enable_dns_support" {
    description = "Enable DNS Support"
    type = bool
    default = true
}

variable "enable_dns_hostnames" {
    description = "Enable DNS Hostnames"
    type = bool
    default = true
}

variable "vpc_name" {
    description = "VPC Name"
    type = string
    default = "my_vpc"
}

variable "igw_name" {
    description = "IGW name"
    type = string
    default = "my_igw"
}

variable "public_subnet_cidr_1" {
    description = "CIDR for public subnet 1"
    type = string
    default = "192.168.1.0/24"
}

variable "public_subnet_cidr_2" {
    description = "CIDR for public subnet 2"
    type = string
    default = "192.168.2.0/24"
}

variable "database_subnet_cidr_1" {
    description = "CIDR for database subnet 1"
    type = string
    default = "192.168.5.0/24"
}

variable "database_subnet_cidr_2" {
    description = "CIDR for database subnet 2"
    type = string
    default = "192.168.6.0/24"
}

variable "public_subnet_1_name" {
    description = "Name of public subnet 1"
    type = string
    default = "public_sn_1"
}

variable "public_subnet_2_name" {
    description = "Name of public subnet 2"
    type = string
    default = "public_sn_2"
}

variable "database_subnet_1_name" {
    description = "Name of database subnet 1"
    type = string
    default = "db_sn_1"
}

variable "database_subnet_2_name" {
    description = "Name of database subnet 2"
    type = string
    default = "db_sn_2"
}

variable "map_public_ip_on_launch" {
    description = "map ip or not"
    type = bool
    default = null
}

variable "public_route_table_tag" {
    description = "tag name of route table"
    type = string
    default = "my_public_route_table"
}

variable "db_route_table_tag" {
    description = "tag name of db route table"
    type = string
    default = "my_db_route_table"
}
