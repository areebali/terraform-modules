####VPC Creation####
resource "aws_vpc" "myVPC" {
    cidr_block = var.vpc_cidr
    enable_dns_support = var.enable_dns_support
    enable_dns_hostnames = var.enable_dns_hostnames
    tags = {
    Name = var.vpc_name
  }
}

####Internet Gateway####
resource "aws_internet_gateway" "myIGW" {
    vpc_id = aws_vpc.myVPC.id
    tags = {
        "Name" = var.igw_name
    }
}

####Public Subnet####
resource "aws_subnet" "public_subnet_1" {
    vpc_id = aws_vpc.myVPC.id
    cidr_block = var.public_subnet_cidr_1
    availability_zone = data.aws_availability_zones.available_list.names[0]
    map_public_ip_on_launch = var.map_public_ip_on_launch
    tags = {
        Name = var.public_subnet_1_name
  }
}

resource "aws_subnet" "public_subnet_2" {
    vpc_id = aws_vpc.myVPC.id
    cidr_block = var.public_subnet_cidr_2
    availability_zone = data.aws_availability_zones.available_list.names[1]
    map_public_ip_on_launch = var.map_public_ip_on_launch
    tags = {
        Name = var.public_subnet_2_name
  }  
}

####Private Subnet####
resource "aws_subnet" "database_subnet_1" {
    vpc_id = aws_vpc.myVPC.id
    cidr_block = var.database_subnet_cidr_1
    availability_zone = data.aws_availability_zones.available_list.names[0]
    map_public_ip_on_launch = false 
    tags = {
        Name = var.database_subnet_1_name
  }
}

resource "aws_subnet" "database_subnet_2" {
    vpc_id = aws_vpc.myVPC.id
    cidr_block = var.database_subnet_cidr_2
    availability_zone = data.aws_availability_zones.available_list.names[1]
    map_public_ip_on_launch = false 
    tags = {
        Name = var.database_subnet_2_name
  }
}

####Public Route Table####
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.myVPC.id

  route = []

  tags = {
    Name = var.public_route_table_tag
  }
}

resource "aws_route" "public_route" {
    route_table_id = aws_route_table.public_route_table.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myIGW.id
}

####Database Route Table####
resource "aws_route_table" "database_route_table" {
  vpc_id = aws_vpc.myVPC.id

  tags = {
    Name = var.db_route_table_tag
  }
}

####Route Table Associations####
resource "aws_route_table_association" "public_route_table_association_1" {
  subnet_id = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_route_table_association_2" {
  subnet_id = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "database_route_table_association_1" {
  subnet_id = aws_subnet.database_subnet_1.id
  route_table_id = aws_route_table.database_route_table.id
}

resource "aws_route_table_association" "database_route_table_association_2" {
  subnet_id = aws_subnet.database_subnet_2.id
  route_table_id = aws_route_table.database_route_table.id
}

####Security Group####
resource "aws_security_group" "MySG" {
  name        = "my_security_group"
  description = "Allow all inbound traffic"
  vpc_id      = aws_vpc.myVPC.id

  ingress {
    description      = "All traffic"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_all"
  }
}