#USE Module with the given parameters
```hcl

provider "aws" {
  region = "us-east-1"
}

module "MSSql" {
  source = "../mssql"
  engine_name = "sqlserver-ex"
  identifier_prefix_name = "my-mssql"
  db_name = null
  username = "root"
  password = "Opstree#12345"
  delete_automated_backups = false
  skip_final_snapshot = true
  multi_az = false
  public_access = true
  instance_class = "db.t3.medium"
  allocated_storage = 20
  primary_subnet_name = "db_sn_1"
  database_subnet_ids = ["subnet-0aa2d68e6ecb9db24", "subnet-0f644290dea869702"]
  database_security_groups = ["sg-079beb53be31a62eb"]
}

```